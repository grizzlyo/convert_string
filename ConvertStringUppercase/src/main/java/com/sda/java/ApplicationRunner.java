package com.sda.java;

import java.util.Arrays;
import java.util.Scanner;

public class ApplicationRunner
{
    public static void main( String[] args )
    {
        Scanner s = new Scanner(System.in);
        System.out.println("Insert the String: ");
        String sir = s.nextLine();

        Logic logic = new Logic();

        System.out.println("Output String: ");
        System.out.println(Arrays.toString(logic.convertString(sir)));

        System.out.println("Output String for Method2: ");
        System.out.println(logic.convert(sir));

    }
}
