package com.sda.java;

public class Logic {

    public String[] convertString(String toConvert) {
        String[] arr = toConvert.split(" ");
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].equals(arr[i].toLowerCase())) {
                arr[i] = arr[i].toUpperCase();
            } else if (arr[i].equals(arr[i].toUpperCase())) {
                arr[i] = arr[i].toLowerCase();
            }
        }
        return arr;
    }

    public String convert(String toConvert) {
        StringBuilder builder = new StringBuilder(toConvert);
        for (int i = 0; i < builder.length(); i++) {
            Character c = builder.charAt(i);
            if (Character.isLowerCase(c)) {
                builder.replace(i, i + 1, Character.toUpperCase(builder.charAt(i)) + "");
            } else if (Character.isUpperCase(c)) {
                builder.replace(i, i + 1, Character.toLowerCase(builder.charAt(i)) + "");
            }
        }
        String convertString = builder.toString();
        return convertString;
    }
}
