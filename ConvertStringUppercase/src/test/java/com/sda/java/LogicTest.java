package com.sda.java;

import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Test;

public class LogicTest {

    @Test
    public void convertStringTest() {
        System.out.println("The convertString method was called!");

        Logic logic = new Logic();
        String test = "ANA are MERE";

        String[] actual = logic.convertString(test);
        String[] expected = {"ana", "ARE", "mere"};

        Assert.assertArrayEquals(expected, actual);
    }
    @Test
    public void convertTest (){
        System.out.println("The convert method was called!");

        Logic logic = new Logic();
        String test = "ana ARE MERE bune";

        String actual = logic.convert(test);
        String expected = "ANA are mere BUNE";

        Assert.assertEquals(expected, actual);
    }
}


